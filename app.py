#Authors: Bryce Hansen, Chonghuan Xia, An Zhu


from flask import Flask, jsonify, request, redirect
from urllib.parse import urlparse
from blockchain.Min_Chain import Chain 
import subprocess

import requests

# Instantiate our Node
app = Flask(__name__)

# Instantiate the chain
blockchain = Chain()
script = '''
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
			<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		 '''

@app.route('/', methods=['GET'])
def index():
	html = '''<!DOCTYPE html>
			<html>
				<body>
					<div class="container">
					<h3 style="text-align:center;">Show all Users</h3>
					<table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Number</th>
                        <th>Name</th>
                        <th>Reference list</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td colspan="3" style="text-align:center;">show all Males</td>
                      </tr>'''
	for person in blockchain.getChain():
		if person.get_gender() == "male":
			html += "<tr><td><a href=\"/update?id="+str(person.get_index())+"\"><button>"+str(person.get_index())+"</button></td><td>"+person.get_name()+"</td><td>"+str(person.get_preference_table())+"</td></tr>"
	html += '''<tr>
                        <td colspan="3" style="text-align:center;">show all Females</td>
                      </tr>'''
	for person in blockchain.getChain():
		if person.get_gender() == "female":
			html += "<tr><td><a href=\"/update?id="+str(person.get_index())+"\"><button>"+str(person.get_index())+"</button></td><td>"+person.get_name()+"</td><td>"+str(person.get_preference_table())+"</td></tr>"
	html += '''</tbody></table>
				</div>
				<div><a href="/create"><button>Register new User</button></a></div>
				<div><a href="/reset"><button>Reset Chain (in order to do the match function)</button></a></div>
				<div><a href="/match"><button>Find stable couple</button></a></div>
				</body>
			</html>
			'''
	return html + script

@app.route('/create', methods=['GET'])
def showForm():
	html = '''
			<!DOCTYPE html>
			<html>
			<body>
				<div class="container">
					<h2>Register a new User</h2>
					<form action="/create" method="post">
					  <label for="ssn">SSN:</label><br>
					  <input type="text" id="ssn" name="ssn"/><br>

					  <label for="name">Name:</label><br>
					  <input type="text" id="name" name="name"/><br>

					  <label>Gender:</label><br>
					  <input type="radio" id="male" name="gender" value="male">
					  <label for="male">Male</label><br>
					  <input type="radio" id="female" name="gender" value="female">
					  <label for="female">Female</label><br>
					  
					  <label for="preference">Preference Number (comma seperated):</label><br>
					  <input type="text" id="preference" name="preference"/><br>

					  <br>
					  <input type="submit" value="Submit">
					</form> 

				</div>
			</body>
		</html>
	'''
	return html + script

@app.route('/create', methods=['POST'])
def create():
	values = request.form
	name = values['name']
	ssn = values['ssn']
	gender = values['gender']
	p_l = values['preference'].split(',')
	preference = []
	for i in range(len(p_l)):
		if p_l[i] :
			if p_l[i].strip().isdigit():
				preference.append(int(p_l[i]))
			else:
				return "Error! Preference list has non-integer object"
	flag = blockchain.add_block(name,ssn,gender,preference)
	if flag == True:
		return redirect("/", code=302)
	return "Error! "+flag

@app.route('/update', methods=['GET'])
def updateForm():
	data = request.args
	index = data['id']
	block = blockchain.get_block(int(index))
	html = '''
			<!DOCTYPE html>
			<html>
			<body>
				<div class="container">
					<h2>Update User's Preference list</h2>
					<form action="/update" method="post">
					  <label for="name">Name:</label><br>
					  <input type="text" id="name" name="name" value="''' + block.get_name()+'''" readonly/><br>

					  <input type="hidden" name="index" value="''' + index+'''"/><br>
					  <input type="hidden" name="gender" value="''' + block.get_gender()+'''"/><br>

						<label for="preference0">Current Preference list:</label><br>
					  <input type="text" id="preference0" name="preference0" value="''' + str(block.get_preference_table())+'''" readonly/><br>
					  
					  <label for="preference">New Preference Number (comma seperated):</label><br>
					  <input type="text" id="preference" name="preference"/><br>

					  <br>
					  <input type="submit" value="Submit">
					</form> 

				</div>
			</body>
		</html>
	'''
	return html + script

@app.route('/update', methods=['POST'])
def update():
	values = request.form
	index = int(values['index'])
	gender = values['gender']
	p_l = values['preference'].split(',')
	preference = []
	for i in range(len(p_l)):
		if p_l[i]:
			if p_l[i].strip().isdigit():
				preference.append(int(p_l[i]))
			else:
				return "Error! Preference list has non-integer object"
	flag = blockchain.updatePreferenceTable(gender, index, preference)
	if flag == True:
		return redirect("/", code=302)
	return "Error! "+flag
	# return values

@app.route('/match', methods=['GET'])
def match():
	re = blockchain.pairing()
	if re == False:
		return "Error! The number of males doesn't match the number of females or the preference list is not updated to match the number of males/females."
	return re.replace("\n","<br/>")


@app.route('/reset', methods=['GET'])
def reset():
	subprocess.run(["scp", "BlockChain1.json", "BlockChain.json"])
	subprocess.run(["scp", "PreferenceTable1.json", "PreferenceTable.json"])
	global blockchain
	blockchain = Chain()
	return redirect("/", code=302)

