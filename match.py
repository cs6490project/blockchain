
# Implementation of the stable marriage problem using Gale-Shapley algorithm.
# Complexity Upper Bound : O(n^2)

import json

class Match (object):
	PreferenceFile = "PreferenceTable.json"
	PreferenceTableForMale = {}
	PreferenceTableForFemale = {}
	MatchForMale = {}
	MatchForFemale = {}

	def __init__(self,test=False, preferenceTable={}):
		self.test = test
		if test:
			self.PreferenceTableForMale = preferenceTable['male']
			self.PreferenceTableForFemale = preferenceTable['female']


	# private function to get preference tables
	def __getPreferenceTable(self):
		with open(self.PreferenceFile,'r') as f:
			data = f.read()
		data = json.loads(data)
		self.PreferenceTableForMale = data['male']
		self.PreferenceTableForFemale = data['female']
		# print(self.PreferenceTableForMale)
		# print(self.PreferenceTableForFemale)

	#  private function to initiate match tables 
	def __initMatchTable(self):
		for person in self.PreferenceTableForMale.keys():
			self.MatchForMale[person] = None
		for person in self.PreferenceTableForFemale.keys():
			self.MatchForFemale[person] = None
		# print(self.MatchForMale)
		# print(self.MatchForFemale)

	# private function to get a unpaired person
	# return person id if there is one
	#        or False
	def __getUnpairedPerson(self, matchTable):
		for person in matchTable.keys():
			if not matchTable[person]:
				return person
		return False

	#  private function to compare the rank of two persons in the same preference list
	def __compareRank (self, preferenceTable, person1, person2):
		for i in range(len(preferenceTable)):
			if preferenceTable[i] == int(person1):
				return True
			elif preferenceTable[i] == int(person2):
				return False
		return False


	def Gale_Shapley(self):
		# get preference table
		if not self.test:
			self.__getPreferenceTable()
		#  initiate Match tables
		self.__initMatchTable()

		# continuely check males until there is no unpaired one
		while 1:
			# find the unpaired male
			male = self.__getUnpairedPerson(self.MatchForMale)
			# print(male)
			if not male:
				break 
			index = 0
			# check each female in the preference list of the current male
			while index < len(self.PreferenceTableForMale[male]):
				#  get the female id
				female = str(self.PreferenceTableForMale[male][index])
				# to match them if the female is not paired
				if not self.MatchForFemale[female]:
					self.MatchForMale[male] = female
					self.MatchForFemale[female] = male
					break
				# if the female is matched, compare the rank of the two males in the female's preference list
				# if the female prefers the current male, then replace match table with this male
				else:
					male2 = self.MatchForFemale[female]
					if self.__compareRank(self.PreferenceTableForFemale[female],male,male2):
						self.MatchForFemale[female] = male
						self.MatchForMale[male] = female
						self.MatchForMale[male2] = None
						break
				index += 1

	def printMatchResult(self):
		result =""
		result += "Match result for Male:\n"
		for male, female in self.MatchForMale.items():
			result += "\nMan "+male+" -> Woman "+female
		
		result += "\n\nMatch result for Female:"
		for female, male in self.MatchForFemale.items():
			result += "\nWoman "+female+" -> Man "+male
		return result


if __name__ == '__main__':
	m = Match()
	m.Gale_Shapley()
	print(m.printMatchResult())