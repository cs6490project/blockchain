import hashlib
import datetime
import json
import time
from Block import Block
from match import Match


class Chain(object):
    BlockChainFile = "BlockChain.json"        #json files play a role as database
    PreferenceFile = "PreferenceTable.json"

    # each time user runs the app, sync the data 
    def __init__(self, test=False):
        self.test = test
        if test:
            self.BlockChainFile = "BlockChaintest.json"        #json files play a role as database
            self.PreferenceFile = "PreferenceTabletest.json"
        with open(self.BlockChainFile,'r') as f:
            data = f.read()
        chaindata = json.loads(data)

        with open(self.PreferenceFile,'r') as f:
            data = f.read()
        preferencedata = json.loads(data)

        self.ssnSet = {}
        self.PreferenceTable = preferencedata
        self.block_chain = []
        for item in chaindata['data']:
            preference_table = self.PreferenceTable[item['gender']][str(item['index'])]
            self.ssnSet[item['ssn']] = item['index']
            self.block_chain.append(Block(item['index'],
                                          item['ssn'],
                                          item['name'],
                                          item['gender'],
                                          preference_table,
                                          item['myhash'],
                                          item['previous_hash']))
        
        # self.block_chain = [self.get_root()]
        #add whatever you need for fast search here

    # def get_root(self):
    #     return Block(0,datetime.datetime.now(),'Root','Root')

    def add_block(self,name,ssn,gender,preference_table):
        if ssn in self.ssnSet.keys():
            return "You have been registered."
        previous_hash = ""
        myhash = ""
        if self.test:
            self.block_chain.append(Block(len(self.block_chain),
                     ssn,
                     name,
                     gender, 
                     preference_table,
                     myhash,
                     previous_hash
                     ))
            return True
        oppsite = "male" if gender=='female' else "female"
        if len(preference_table) != len(self.PreferenceTable[oppsite]):
            return "The length of preference list is wrong"
        for i in preference_table:
            if str(i) not in self.PreferenceTable[oppsite].keys():
                return "Preference list is wrong due to a wrong index number."
        if len(self.block_chain) > 0:
            previous_hash = self.block_chain[len(self.block_chain)-1].get_myhash()
        key = hashlib.sha256()
        key.update(str(len(self.block_chain)).encode('utf-8'))
        key.update(str(ssn).encode('utf-8'))
        key.update(str(name).encode('utf-8'))
        key.update(str(gender).encode('utf-8'))
        key.update(str(previous_hash).encode('utf-8'))
        myhash = key.hexdigest()
        block = Block(len(self.block_chain),
                     ssn,
                     name,
                     gender, 
                     preference_table,
                     myhash,
                     previous_hash
                     )
        self.block_chain.append(block)
        if self.verify():
            self.PreferenceTable[gender][str(block.get_index())] = preference_table
            self.ssnSet[block.get_ssn()] = block.get_index()
            self.__updatePreferencefile()
            self.__updateChainfile()
            return True
        else:
            del self.block_chain[-1]
            return "Block verify failed."



    def __updatePreferencefile(self):
        with open(self.PreferenceFile,'w') as f:
            json.dump(self.PreferenceTable, f)

    def __updateChainfile(self):
        data = []
        for block in self.block_chain:
            data.append({"index":block.get_index(),
                         "ssn":block.get_ssn(),
                         "name": block.get_name(),
                         "gender":block.get_gender(),
                         "myhash":block.get_myhash(),
                         "previous_hash":block.get_previous_hash()})
        d = {}
        d['data'] = data
        with open(self.BlockChainFile,'w') as f:
            json.dump(d, f)

    def updatePreferenceTable(self, gender, index, table):
        oppsite = "male" if gender=='female' else "female"
        if len(table) != len(self.PreferenceTable[oppsite]):
            return "The length of preference list is wrong"
        for i in table:
            if str(i) not in self.PreferenceTable[oppsite].keys():
                return "Preference list is wrong due to a wrong index number."

        self.PreferenceTable[gender][str(index)] = table
        self.block_chain[index].set_preference_table(table)
        self.__updatePreferencefile()
        return True
    
    def get_size(self):
        return len(self.block_chain)
    
    def verifyblock(self,index):
        # print(index)
        flag = True
        if self.block_chain[index].get_index() != index:
            flag = False
            print('Wrong index at '+ str(index))
        if index >0 and self.block_chain[index-1].get_hash() != self.block_chain[index].get_previous_hash():
            flag = False
            print('Wrong previous hash at ' + str(index))
        if self.block_chain[index].get_myhash() != self.block_chain[index].get_hash():
            flag = False
            print('Wrong hash at ' + str(index))
        # if self.block_chain[index-1].timestamp > self.block_chain[index].timestamp:
        #     flag = False
        #     print('Wrong Time at ' + str(index))
        return flag
    
    def verify(self):
        flag = True
        for i in range(0, len(self.block_chain)):
            temp_flag = self.verifyblock(i)
            if(temp_flag == False):
                flag = False
        return flag

    def get_block(self,index):
        return self.block_chain[index]

    #do a fast search based on ssn (users use ssn to login):
    def search_block(self, ssn):
        return self.block_chain[self.ssnSet[ssn]]

    #return a stable pairing table.
    def pairing(self):
        if len(self.PreferenceTable['female']) != len(self.PreferenceTable['male']):
            return False
        for table in self.PreferenceTable['male'].values():
            if len(table) != len(self.PreferenceTable['female']):
                return False

        for table in self.PreferenceTable['female'].values():
            if len(table) != len(self.PreferenceTable['male']):
                return False
        start = time.time()
        match = Match()
        match.Gale_Shapley()
        end = time.time()
        if self.test:
            return end-start
        return match.printMatchResult()

    def getChain(self):
        return self.block_chain
    #def fork(self, head = 0):
     #       if head == 0:
      #          return copy.deepcopy(self)
       #     else:
        #        duplica = copy.deepcopy(self)
         #       duplica.block_chain = duplica.block_chain[0:head+1]
          #      return duplica
            
    #def min_chains(self,another):
     #   min_chain_size = min(self.get_size(),another.get_size())
      #  for i in range(1, min_chain_size):
       #     if self.block_chain[i] != another.block_chain[i]
        #        return self.fork(i-1)
         #   return self.fork(min_chain_size)

if __name__ == '__main__':
    chain = Chain()
    # chain.add_block('Jenny',5644645214,"female",[3,0,2,1,4])
    # flag = chain.verify()
    # print(flag)
    # block = chain.search_block(1234567)
    # print(block.get_name())
    # chain.updatePreferenceTable('male',1,[7,5,9,8,6])
    chain.pairing()
