import blockchain
import match
from infofactory import InfoFac

#this function will take two block_chains an fill them
def make_chains(male_chaine, female_chaine, number_males, number_females):
    infofactory = InfoFac(number_males,number_females)
    for i in range(number_males):
        data = infofactory.get_male()
        male_chaine.add_block(**data)
    for i in range(number_females):
        data = infofactory.get_female()
        female_chaine.add_block(**data)

#five males and five females
m_chain = blockchain
f_chain = blockchain
make_chains(m_chain,f_chain,5,5)


#test
#for i in range(6):
#    print(m_chain.chain[i])
#print(m_chain.hashmap)
#male = m_chain.get_block(0)
#print(male)