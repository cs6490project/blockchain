import json
import time
from Block import Block
from match import Match
from infofactory import InfoFac

class TestChain(object):
	BlockChainFile = "BlockChaintest.json"        #json files play a role as database
	PreferenceFile = "PreferenceTabletest.json"
	block_chain = []
	ssnSet = {}
	PreferenceTable = {"male":{},"female":{}}

	def add_block(self,name,ssn,gender,preference_table):
		if str(ssn) in self.ssnSet.keys():
			return "You have been registered."
		previous_hash = ""
		myhash = ""

		block = Block(len(self.block_chain),
                 str(ssn),
                 name,
                 gender, 
                 preference_table,
                 myhash,
                 previous_hash
                 )
		self.block_chain.append(block)
		self.PreferenceTable[gender][str(block.get_index())] = preference_table
		self.ssnSet[block.get_ssn()] = block.get_index()
		return True

	def getPreferenceTable(self):
		return self.PreferenceTable

	def getChain(self):
		return self.block_chain

	def writePreferencefile(self):
		with open(self.PreferenceFile,'w') as f:
			json.dump(self.PreferenceTable, f)
	

	def writeChainfile(self):
		data = []
		for block in self.block_chain:
			data.append({"index":block.get_index(),
                         "ssn":block.get_ssn(),
                         "name": block.get_name(),
                         "gender":block.get_gender(),
                         "myhash":block.get_myhash(),
                         "previous_hash":block.get_previous_hash()})
		d = {}
		d['data'] = data
		with open(self.BlockChainFile,'w') as f:
			json.dump(d, f)

if __name__ == '__main__':
	number_males = number_females = 2000
	start = time.time()
	infofactory = InfoFac(number_males,number_females)
	testChain = TestChain()
	for i in range(number_males):
		data = infofactory.get_male()
		testChain.add_block(**data)
	for i in range(number_females):
		data = infofactory.get_female()
		testChain.add_block(**data)
	end = time.time()
	print("generate the chain costs ",end-start, 's')
	preference_table = testChain.getPreferenceTable()
	chain = testChain.getChain()

	match = Match(True,preference_table)
	start = time.time()
	for block in chain:
		table = block.get_preference_table()
	match.Gale_Shapley()
	end = time.time()
	print("Go through the chain:",str(number_males), "blocks costs", end-start, 's')
    
	match = Match(True,preference_table)
	start = time.time()
	match.Gale_Shapley()
	end = time.time()
	print("our method:",str(number_males), "blocks costs", end-start, 's')

    
