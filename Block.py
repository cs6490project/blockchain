# Block class reprents a person. 
# The person uses ssn to register to make sure no duplicate person in the chain.

import hashlib


class Block():
    def __init__(self, index, ssn, name, gender, preference_table, myhash,previous_hash):
        self.__index = index
        self.__ssn = ssn
        self.__name = name
        self.__gender = gender
        self.__preference_table = preference_table
        self.__previous_hash = previous_hash
        self.__myhash = myhash

    def get_hash(self):
        key = hashlib.sha256()
        key.update(str(self.__index).encode('utf-8'))
        key.update(str(self.__ssn).encode('utf-8'))
        key.update(str(self.__name).encode('utf-8'))
        key.update(str(self.__gender).encode('utf-8'))
        #encrypt other data here for search and compare
        key.update(str(self.__previous_hash).encode('utf-8'))
        return key.hexdigest()

    def get_index(self):
        return self.__index

    def get_ssn(self):
        return self.__ssn

    def get_name(self):
        return self.__name

    def get_gender(self):
        return self.__gender

    def get_previous_hash(self):
        return self.__previous_hash

    def get_myhash(self):
        return self.__myhash

    def get_preference_table(self):
        return self.__preference_table

    def set_preference_table(self,table):
        self.__preference_table = table
