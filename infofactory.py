print("Info factory impoted successfully")
import copy
import random

class InfoFac():
    def __init__(self, number_males = 5, number_females = 5):
        self.male_names = ['Adam','Smith','Lion','MorningStar','Superman']
        self.femal_names = ['Eve','Trudy','Alice','Joe','Jenny']
        self.male_count = number_males
        self.female_count = number_females
        self.male_ssntalbe = []
        self.female_ssntable = []
        self.male_preferencetable = list(range(number_males,number_females+number_males))
        self.female_preferencetable = list(range(number_males))
        for i in range(self.male_count):
            r = random.randint(100000000,200000000)
            while r in self.male_ssntalbe:
                r = random.randint(100000000,200000000)
            self.male_ssntalbe.append(r)
        for i in range(self.female_count):
            r = random.randint(200000000,300000000)
            while r in self.female_ssntable:
                r = random.randint(200000000,300000000)
            self.female_ssntable.append(r)
    def get_male(self):
        name = self.male_names[self.male_count%5]
        uid = self.male_ssntalbe[self.male_count -1]
        preferencetable = copy.deepcopy(self.male_preferencetable)
        random.shuffle(preferencetable)
        self.male_count -= 1
        return {'name':name,'ssn':uid,'gender':'male','preference_table':preferencetable}
    def get_female(self):
        name = self.femal_names[self.female_count%5]
        uid = self.female_ssntable[self.female_count -1]
        preferencetable = copy.deepcopy(self.female_preferencetable)
        random.shuffle(preferencetable)
        self.female_count -= 1
        return {'name':name,'ssn':uid,'gender':'female','preference_table':preferencetable}
    
    #Example of make chian of certain size
def make_chains(number_males, number_females,block_chain):
    infofactory = InfoFac(number_males,number_females)
    for i in range(number_males):
        data = infofactory.get_male()
        print(data)
            #block_chain.add_block(**data)
    for i in range(number_females):
        data = infofactory.get_female()
        print(data)
            #block_chain.add_block(**data)
if __name__ == '__main__':
    make_chains(10000,10000,1)